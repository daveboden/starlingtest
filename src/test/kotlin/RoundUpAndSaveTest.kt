import org.junit.Assert.assertEquals
import org.junit.Test
import java.math.BigDecimal
import java.time.Instant

class RoundUpAndSaveTest {

    @Test
    fun testDecimalsRoundupFromTheSpecificationExample() {
        assertEquals(
                BigDecimal("-1.58"),
                calculateRoundingAmountTotal(
                        listOf(
                                BigDecimal("-4.35"),
                                BigDecimal("-5.20"),
                                BigDecimal("-0.87")
                        )
                )
        )
    }

    @Test
    fun testExampleTransactionsRoundup() {
        val transactions = object {}::class.java.classLoader.getResource("example_transactions.json").readText()
        val amounts = getAmountsToRoundUpFromJson(transactions, Instant.parse("2018-09-20T00:00:00Z"))
        //Amount for this example confirmed visually in Excel.
        assertEquals(19, amounts.size)
        val total = calculateRoundingAmountTotal(amounts)
        assertEquals(BigDecimal("-10.83"), total)
    }
}
