import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.httpPut
import org.json.JSONArray
import org.json.JSONObject
import java.math.BigDecimal
import java.math.RoundingMode
import java.time.Instant
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import java.util.UUID

const val STARLING_URL_BASE = "https://api-sandbox.starlingbank.com/api/v1"
const val STARLING_ACCESS_TOKEN = "cCaywSpsCVlgSrNC9hdYc378mRPmuEq83mDuiUY64aeMMhmSP2uz9zXhWhwsAavB"

val headers = mapOf(
        "Accept" to "application/json",
        "Authorization" to "Bearer $STARLING_ACCESS_TOKEN",
        "User-Agent" to "David Boden"
)
val headersWrite = headers.plus(
        "Content-Type" to "application/json"
)

fun main(args: Array<String>) {
    val transactions = fetchTransactionsJsonFromApi()

    /*
    Uses current Instant.now(). Don't use in tests. In a real application we'd use the concept of the current business
    day for the user rather than relying on a rolling 7 day window.
     */
    val oneWeekAgo =  Instant.now().minus(7, ChronoUnit.DAYS)

    val amounts = getAmountsToRoundUpFromJson(transactions, oneWeekAgo)

    val roundingTotal = calculateRoundingAmountTotal(amounts)

    createSavingsGoalAndAddMoney(roundingTotal)
}

fun fetchTransactionsJsonFromApi(): String =
        "$STARLING_URL_BASE/transactions".httpGet().header(headers).responseString().third.get()

fun getAmountsToRoundUpFromJson(json: String, fromTimestamp: Instant): List<BigDecimal> {
    val transactions: JSONArray = JSONObject(json).getJSONObject("_embedded").getJSONArray("transactions")

    val amounts = mutableListOf<BigDecimal>()

    for(i in 0 until transactions.length()) {
        val t = transactions.getJSONObject(i)
        val created = Instant.from(DateTimeFormatter.ISO_DATE_TIME.parse(t.getString("created")))
        if(t.getString("direction") == "OUTBOUND" && fromTimestamp.isBefore(created)) {
            amounts.add(t.getBigDecimal("amount"))
        }
    }
    return amounts
}

private fun calculateRoundingAmountRequired(amount: BigDecimal) = amount.setScale(0, RoundingMode.UP) - amount

fun calculateRoundingAmountTotal(amounts: List<BigDecimal>): BigDecimal =
    amounts.fold(BigDecimal.ZERO) {acc: BigDecimal, a: BigDecimal -> acc + calculateRoundingAmountRequired(a)}

fun createSavingsGoalAndAddMoney(amount: BigDecimal) {
    val savingsGoalUid = UUID.randomUUID()
    val transferUid = UUID.randomUUID()

    val savingsGoalDetails = JSONObject(
            mapOf(
                "name" to "Lunar trip with artists",
                "currency" to "GBP",
                "target" to mapOf(
                        "currency" to "GBP",
                        "minorUnits" to 50000
                )
            )
    ).toString()

    val amountInMinorUnits = (amount * BigDecimal(100)).abs().setScale(0, RoundingMode.UNNECESSARY)

    val transferDetails = JSONObject(
            mapOf(
                "amount" to mapOf(
                        "currency" to "GBP",
                        "minorUnits" to amountInMinorUnits
                )
            )
    ).toString()

    println("Creating a savings goal: $savingsGoalUid")
    "$STARLING_URL_BASE/savings-goals/$savingsGoalUid"
            .httpPut().header(headersWrite).body(savingsGoalDetails).response().third.get()

    println("Transferring amount $amountInMinorUnits to savings goal $savingsGoalUid with transfer id: $transferUid")
    "$STARLING_URL_BASE/savings-goals/$savingsGoalUid/add-money/$transferUid"
            .httpPut().header(headersWrite).body(transferDetails).response().third.get()

}
